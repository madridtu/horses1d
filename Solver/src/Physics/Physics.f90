module Physics

#ifdef ADVECTION
    use PhysicsAdvection
#elif BURGERS
    use PhysicsBurgers
#else
    use PhysicsDefault
#endif




end module Physics

