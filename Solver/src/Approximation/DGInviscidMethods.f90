module DGFirstOrderMethods
   use SMConstants
   use FaceClass
   use Element1DClass
   use Physics
   use NodesAndWeights_class
   implicit none
!
!  *******************************************************************
   private
   public FirstOrderMethod_t , StandardDG_t , OverIntegrationDG_t , SplitDG_t
   public FirstOrderMethod_Initialization
!  *******************************************************************
!
!                                *************************
   integer, parameter         :: STR_LEN_FIRSTORDER = 128
!                                *************************
!
!  *******************************************************************
   type FirstOrderMethod_t
      character(len=STR_LEN_FIRSTORDER)         :: method
      procedure(RiemannSolverFunction), pointer, nopass :: RiemannSolver => NULL()
      contains
         procedure, non_overridable :: QDotFaceLoop => BaseClass_QDotFaceLoop
         procedure, non_overridable :: RiemannFlux => FirstOrderMethod_RiemannFlux
         procedure                  :: QDotVolumeLoop => BaseClass_QDotVolumeLoop
         procedure, non_overridable :: Describe => FirstOrderMethod_describe
   end type FirstOrderMethod_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(FirstOrderMethod_t) :: StandardDG_t
      contains
         procedure ::  QDotVolumeLoop => StdDG_QDotVolumeLoop
   end type StandardDG_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(FirstOrderMethod_t) ::  OverIntegrationDG_t
      contains
         procedure ::  QDotVolumeLoop => OIDG_QDotVolumeLoop
   end type OverIntegrationDG_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(FirstOrderMethod_t) ::  SplitDG_t
      real(kind=RP)              :: alpha
      contains
         procedure ::  QDotVolumeLoop => SplitDG_QDotVolumeLoop
   end type SplitDG_t
!
!  ========  
   contains
!  ========  
!
      function FirstOrderMethod_Initialization() result( FirstOrderMethod )
         implicit none
         class(FirstOrderMethod_t), pointer        :: FirstOrderMethod
!
!        --------------------------------------
!           Prepare the first order method
!        --------------------------------------
!
         if ( trim( Setup % inviscid_discretization ) .eq. "Standard" ) then
            
            allocate(StandardDG_t   :: FirstOrderMethod)

         elseif ( trim( Setup % inviscid_discretization ) .eq. "Over-Integration" ) then 

            allocate(OverIntegrationDG_t  :: FirstOrderMethod)

         elseif ( trim( Setup % inviscid_discretization ) .eq. "Split") then
      
            allocate(SplitDG_t      :: FirstOrderMethod)
      
         else
            write(STD_OUT , *) "Method ", trim(Setup % inviscid_discretization), " not implemented yet."
            STOP "Stopped in line 78 of file FirstOrderMethods.f90"

         end if

         FirstOrderMethod % method = trim( Setup % inviscid_discretization )


         select type (FirstOrderMethod)
            type is (StandardDG_t)

            type is (OverIntegrationDG_t)
   
            type is (SplitDG_t)

            class default
         end select

!        Set the Riemann flux
         if (trim( Setup % riemann_Solver ) .eq. "Roe") then
            FirstOrderMethod % RiemannSolver => RoeFlux

         elseif ( trim ( Setup % riemann_solver) .eq. "ECON") then
            FirstOrderMethod % RiemannSolver => ECONFlux

         elseif ( trim ( Setup % riemann_solver ) .eq. "LLF") then
            FirstOrderMethod % RiemannSolver => LocalLaxFriedrichsFlux

         else
            write(STD_OUT , *) "Solver ", trim ( Setup % riemann_solver) ," not implemented yet."
            Stop "Stopped in line 107 of file FirstOrderMethods.f90"
         end if

         call FirstOrderMethod % describe
 

      end function FirstOrderMethod_Initialization

      subroutine BaseClass_QDotFaceLoop( self , face )
         use MatrixOperations
         implicit none
         class(FirstOrderMethod_t)          :: self
         class(Face_t), pointer             :: face
         real(kind=RP), dimension(NEC)      :: Fstar
!
!        -------------------------------------------
!           This is a standard fluxes term
!        -------------------------------------------
!
!        Compute the averaged flux
         Fstar = self % RiemannFlux( face )
!
!        Perform the loop in both elements
         select type ( face )
            type is (Face_t)
               associate(QDot => face % elements(LEFT) % e % QDot, &
                   N=> face % elements(LEFT) % e % spA % N, &
                   e=> face % elements(LEFT) % e)
!     
!                  This (-) sign comes from the equation ut = -fx !
                   QDot = QDot - vectorOuterProduct(e % spA % lb(:,RIGHT) , Fstar) * face % n

               end associate

               associate(QDot => face % elements(RIGHT) % e % QDot, &
                   N=> face % elements(RIGHT) % e % spA % N, &
                   e=> face % elements(RIGHT) % e)

                   QDot = QDot - vectorOuterProduct(e % spA % lb(:,LEFT) , Fstar) * (-1.0_RP * face % n)

               end associate


            type is (BdryFace_t)
               associate(QDot => face % elements(1) % e % QDot , &
                  N => face % elements(1) % e % spA % N , &
                  e => face % elements(1) % e)

                  QDot = QDot - vectorOuterProduct(e % spA % lb(:,face % BCLocation) , Fstar)* face % n

               end associate
            class default
         end select

 
      end subroutine BaseClass_QDotFaceLoop
!
      subroutine BaseClass_QDotVolumeLoop( self , element )
         implicit none
         class(FirstOrderMethod_t)          :: self
         class(Element1D_t)                       :: element
!
!        ---------------------------
!        The base class does nothing
!        ---------------------------
!
      end subroutine BaseClass_QDotVolumeLoop

      subroutine StdDG_QDotVolumeLoop( self , element )
         use MatrixOperations
         implicit none
         class(StandardDG_t)     :: self
         class(Element1D_t)      :: element
!
!        -------------------------------------------
!           The standard DG computes the volume
!        terms as:
!              tr(D)*M*F(Q) = tr(MD)*F(Q)
!        -------------------------------------------
!
!        Compute fluxes
         element % F = inviscidFlux( element % Q )
!
!        Perform the matrix multiplication
         associate( QDot => element % QDot , &
                    MD => element % spA % MD)

         QDot = QDot + Mat_x_Mat_F ( MD , element % F , trA = .true. )

         end associate

      end subroutine StdDG_QDotVolumeLoop

      subroutine OIDG_QDotVolumeLoop( self , element )
         use MatrixOperations
         implicit none
         class(OverIntegrationDG_t)          :: self
         class(Element1D_t)                  :: element
!
!        ---------------------------------------------------
!           The Over-Integration DG computes the volume
!        terms as:
!           tr(tildeM T D) * F(tildeQ)
!        ---------------------------------------------------
!
!        Compute fluxes
         element % F = inviscidFlux( matmul(element % spA % T , element % Q) )
!
!        Perform the matrix multiplication
         associate( QDot => element % QDot , &
                  tildeMTD => element % spA % tildeMTD)

         QDot = QDot + Mat_x_Mat_F ( tildeMTD , element % F , trA = .true. )

         end associate

      end subroutine OIDG_QDotVolumeLoop

      subroutine SplitDG_QDotVolumeLoop( self , element )
         implicit none
         class(SplitDG_t)        :: self
         class(Element1D_t)      :: element

      end subroutine SplitDG_QDotVolumeLoop


      subroutine FirstOrderMethod_describe( self )
         implicit none
         class(FirstOrderMethod_t)        :: self

         write(STD_OUT , * ) "First order method description: "
         write(STD_OUT , '(20X,A,A)') "Method: ", trim( self % method )

         select type ( self ) 
            type is ( StandardDG_t )
   
            type is ( OverIntegrationDG_t )
         
            type is ( SplitDG_t )
               write(STD_OUT , '(20X,A,F10.4)') "Split op. coefficient: " , self % alpha

            class default
      
         end select

      end subroutine FirstOrderMethod_describe
   
      function FirstOrderMethod_RiemannFlux( self , face ) result( val )
         implicit none
         class(FirstOrderMethod_t)        :: self
         class(Face_t), pointer           :: face
         real(kind=RP), dimension(NEC)    :: val
         real(kind=RP), pointer  :: QL(:) , QR(:) , QBdry(:)
         real(kind=RP), pointer  :: n

         select type ( face )
            type is (BdryFace_t)
      
               QBdry => face % elements(1) % e % Qb( : , face % BCLocation  )
               n     => face % n

               val = self % RiemannSolver(QBdry , face % uB , n)
               
            type is (Face_t)
      
               QL => face % elements(LEFT) % e % Qb( : , RIGHT )
               QR => face % elements(RIGHT) % e % Qb( : , LEFT )
               n  => face % n

               val = self % RiemannSolver(QL , QR , n)

            class default
         end select
      end function FirstOrderMethod_RiemannFlux

end module DGFirstOrderMethods
