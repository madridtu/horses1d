module DGBoundaryConditions
   use SMConstants
   use FaceClass
   use ParamfileIO
   implicit none

   private
   public BoundaryCondition_t , DGBoundaryConditions_Associate

   integer, parameter            :: STR_LEN_BC = 128

   type BoundaryCondition_t
      integer        :: marker
      integer        :: type
      class(BoundaryCondition_t), pointer    :: periodicPair => NULL()
      class(Face_t), pointer                 :: face => NULL()
      real(kind=RP), pointer                 :: uBC(:) => NULL()
      real(kind=RP), pointer                 :: gBC(:) => NULL()
      contains
         procedure   :: Associate => DGBoundaryConditions_Associate
         procedure   :: Construct => DGBoundaryConditions_Construct
   end type BoundaryCondition_t

   contains
      subroutine DGBoundaryConditions_Construct( self , marker , face , BCset)
         use Physics
         use Setup_class
         implicit none
         class(BoundaryCondition_t)          :: self
         integer                             :: marker
         class(Face_t), pointer              :: face
         class(BoundaryCondition_t), pointer :: BCset(:)       ! The whole set of BCs
         character(len=STR_LEN_BC)           :: BCType
         character(len=STR_LEN_BC)           :: in_label
         real(kind=RP), allocatable          :: Dirichlet_Value
         integer,       allocatable          :: PeriodicPair

         self % marker = marker
         self % face   => face

         write(in_label , '(A,I0)' ) "#define zone ", marker

         call ReadValueInRegion( trim(Setup % bdry_file) , "Type" , BCType , in_label , "#end")
         call ReadValueInRegion( trim(Setup % bdry_file) , "Value" , Dirichlet_Value , in_label , "#end")
         call ReadValueInRegion( trim(Setup % bdry_file) , "Marker" , PeriodicPair, in_label , '#end')

         if ( trim(BCType) .eq. "Periodic" ) then
            self % type = PERIODIC_BC
      
         elseif ( trim(BCType) .eq. "Dirichlet" ) then
            self % type = DIRICHLET_BC
   
         end if

         if ( self % type .eq. PERIODIC_BC) then
            self % periodicPair => BCset( PeriodicPair )   ! Associate its pair

         elseif (self % type .eq. DIRICHLET_BC) then
            allocate( self % uBC(NEC) )

            self % uBC = Dirichlet_Value
         
         end if
                  

      end subroutine DGBoundaryConditions_Construct

      subroutine DGBoundaryConditions_Associate( self  )
         use Physics
         implicit none
         class(BoundaryCondition_t)       :: self

         select type (f1=>self % face)
            type is (BdryFace_t)
               if (self % type  .eq. PERIODIC_BC) then
!                 -------------------------------
!                    Look for its pairing
!                 -------------------------------
                  select type (f2=>self % periodicPair % face)
                     type is (BdryFace_t)
                        f1 % uB => f2 % elements(1) % e % Qb( : , f2 % BCLocation )  
                        f1 % gB => f2 % elements(1) % e % dQb( : , f2 % BCLocation )  
                  end select
               elseif ( self % type .eq. DIRICHLET_BC) then
!                 -------------------------------
!                    Allocate data and set values
!                 -------------------------------
                        allocate( f1 % uB (NEC) )
                        f1 % uB = self % uBC
                        f1 % gB => f1 % elements(1) % e % dQb( : , f1 % BCLocation )

               end if
         end select

         


      end subroutine DGBoundaryConditions_Associate   


end module DGBoundaryConditions
