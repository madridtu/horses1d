module DGSecondOrderMethods
   use SMConstants
   use FaceClass
   use Element1DClass
   use Physics
   implicit none
!
!  *******************************************************************
   private
   public SecondOrderMethod_t , IPMethod_t , BR1Method_t , LDGMethod_t
   public SecondOrderMethod_Initialization
!  *******************************************************************
!
!                                *************************
   integer, parameter         :: STR_LEN_SECONDORDER = 128
!                                *************************
!
!  *******************************************************************
   type SecondOrderMethod_t
      character(len=STR_LEN_SECONDORDER)     :: method
      contains
         procedure ::   QDotFaceLoop => BaseClass_QDotFaceLoop
         procedure ::   dQFaceLoop => BaseClass_dQFaceLoop
         procedure ::   QDotVolumeLoop => BaseClass_QDotVolumeLoop
         procedure ::   dQVolumeLoop => BaseClass_dQVolumeLoop
         procedure ::   Describe => SecondOrderMethod_describe
   end type SecondOrderMethod_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(SecondOrderMethod_t) :: IPMethod_t
      character(len=STR_LEN_SECONDORDER)        :: subType
      real(kind=RP)                             :: sigma0
      real(kind=RP)                             :: sigma1
      real(kind=RP)                             :: epsilon
      contains
         procedure :: QDotFaceLoop => IP_QDotFaceLoop
         procedure :: QDotVolumeLoop => IP_QDotVolumeLoop
         procedure :: dQVolumeLoop => IP_dQVolumeLoop
   end type IPMethod_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(SecondOrderMethod_t) ::  BR1Method_t
      contains
         procedure ::  QDotFaceLoop => BR1_QDotFaceLoop
         procedure ::  QDotVolumeLoop => BR1_QDotVolumeLoop
         procedure ::  dQFaceLoop => BR1_dQFaceLoop
   end type BR1Method_t
!  *******************************************************
!  -------------------------------------------------------
!  *******************************************************
   type, extends(SecondOrderMethod_t) ::  LDGMethod_t

   end type LDGMethod_t
!  *******************************************************
!
!  ========
   contains
!  ========
!
      function SecondOrderMethod_Initialization() result( SecondOrderMethod )
         implicit none
         class(SecondOrderMethod_t), pointer       :: SecondOrderMethod
!
!        --------------------------------------
!           Prepare the second order method
!        --------------------------------------
!
         if ( trim( Setup % viscous_discretization ) .eq. "IP" ) then

            allocate(IPMethod_t  :: SecondOrderMethod)

         elseif ( trim( Setup % viscous_discretization ) .eq. "BR1" ) then

            allocate(BR1Method_t :: SecondOrderMethod)

         else

            write(STD_OUT , * ) "Method ",trim(Setup % viscous_discretization)," not implemented yet."
            STOP "Stopped in line 81 of DGSecondOrderMethods.f90"

         end if

  
         SecondOrderMethod % method = trim( Setup % viscous_discretization )
            
         select type (SecondOrderMethod)

            type is (IPMethod_t) 

               SecondOrderMethod % sigma0 = Setup % sigma0IP
               SecondOrderMethod % sigma1 = 0.0_RP

               if (trim ( Setup % IPMethod ) .eq. "SIPG" ) then

                  SecondOrderMethod % subType = "SIPG"
                  SecondOrderMethod % epsilon = -1.0_RP

              else if ( trim ( Setup % IPMethod ) .eq. "NIPG") then
      
                  SecondOrderMethod % subType = "NIPG"
                  SecondOrderMethod % epsilon = 1.0_RP

              else if ( trim ( Setup % IPMethod ) .eq. "IIPG") then

                  SecondOrderMethod % subType = "IIPG"
                  SecondOrderMethod % epsilon = 0.0_RP

              else

                  write(STD_OUT , *) "Method ",trim(Setup % IPMethod)," not implemented yet."
                  stop "Stopped in line 113 of DGSecondOrderMethods.f90"

              end if
      
            type is (BR1Method_t)
!           ------------------------------------
!              No extra parameters are needed
!           ------------------------------------
            class default

                write(STD_OUT , *) "Second order method allocation went wrong."
                stop "Stopped."

         end select

         call SecondOrderMethod % describe()
      end function SecondOrderMethod_Initialization

      subroutine BaseClass_QDotFaceLoop( self , face ) 
         implicit none
         class(SecondOrderMethod_t)          :: self
         class(Face_t)                       :: face
!
!        ------------------------------------
!           The base class does nothing.
!        ------------------------------------
!
      end subroutine BaseClass_QDotFaceLoop

      subroutine BaseClass_dQFaceLoop( self , face )  
         implicit none
         class(SecondOrderMethod_t)          :: self
         class(Face_t)                       :: face
!
!        ------------------------------------
!           The base class does nothing.
!        ------------------------------------
!
      end subroutine BaseClass_dQFaceLoop
      
      subroutine BaseClass_QDotVolumeLoop( self , element ) 
         implicit none
         class(SecondOrderMethod_t)          :: self
         class(Element1D_t)                       :: element
!
!        ------------------------------------
!           The base class does nothing.
!        ------------------------------------
!
      end subroutine BaseClass_QDotVolumeLoop

      subroutine BaseClass_dQVolumeLoop( self , element )  
         use MatrixOperations
         implicit none
         class(SecondOrderMethod_t)          :: self
         class(Element1D_t)                       :: element
!
!        ---------------------------------------
!           The base class consists in a 
!           standard DG volume loop for the
!           solution: -int_ l'j u dx
!              val = -tr(MD)*U^{el}
!        ---------------------------------------
!
         associate( dQ => element % dQ , &
                    MD => element % spA % MD, &
                    Q  => element % Q)

         dQ = dQ - Mat_x_Mat_F ( MD , Q , trA = .true. )
         

         end associate
      end subroutine BaseClass_dQVolumeLoop
 
      subroutine SecondOrderMethod_describe( self )
         implicit none
         class(SecondOrderMethod_t)          :: self

         write(STD_OUT , *) "Second order method description: "
         write(STD_OUT , '(20X,A,A)') "Method: ", trim(self % method)
        
         select type (self)
            type is (IPMethod_t)
               write(STD_OUT , '(20X,A,A)') "Sub method: ", trim(self % subType)
               write(STD_OUT,'(20X,A,F10.4)') "Sigma0: " , self % sigma0
               write(STD_OUT,'(20X,A,F10.4)') "Sigma1: " , self % sigma1
               write(STD_OUT,'(20X,A,F10.4)') "Epsilon: " , self % epsilon
            
            type is (BR1Method_t)
!           ---------------------
!              Nothing to add 
!           ---------------------
            class default

         end select

      end subroutine SecondOrderMethod_describe

      subroutine average_dQFaceLoop( self , face )
         use MatrixOperations
         implicit none
         class(SecondOrderMethod_t)             :: self
         class(Face_t)                          :: face
         real(kind=RP), dimension(NEC)          :: ustar
!
!        Compute the averaged flux
         ustar = average_uFlux( face ) 
!
!        Perform the loop in both elements
         select type ( face )
            type is (Face_t)
               associate(dQ => face % elements(LEFT) % e % dQ, &
                   N=> face % elements(LEFT) % e % spA % N, &
                   e=> face % elements(LEFT) % e)

                  dQ = dQ + vectorOuterProduct(e % spA % lb(:,RIGHT) , ustar) * face % n

               end associate

               associate(dQ => face % elements(RIGHT) % e % dQ, &
                   N=> face % elements(RIGHT) % e % spA % N, &
                   e=> face % elements(RIGHT) % e)

                  dQ = dQ + vectorOuterProduct(e % spA % lb(:,LEFT) , ustar) * (-1.0_RP * face % n)

               end associate


            type is (BdryFace_t)
               associate(dQ => face % elements(1) % e % dQ , &
                  N => face % elements(1) % e % spA % N , &
                  e => face % elements(1) % e)

                  if ( face % BCLocation .eq. LEFT ) then
                     dQ = dQ + vectorOuterProduct(e % spA % lb(:,face % BCLocation) , ustar)*face % n
                  elseif (face % BCLocation .eq. RIGHT) then
                     dQ = dQ + vectorOuterProduct(e % spA % lb(:,face % BCLocation) , ustar)* face % n
                  end if
               end associate
            class default
         end select

     end subroutine average_dQFaceLoop
!
!    ************************************************
!           INTERIOR PENALTY FLUXES 
!    ************************************************
!
     subroutine IP_dQVolumeLoop( self , element )
         implicit none
         class(IPMethod_t)             :: self
         class(Element1D_t)            :: element

         element % dQ = element % dQ + matmul( element % spA % MD , element % Q )

     end subroutine IP_dQVolumeLoop

     subroutine IP_QDotFaceLoop( self , face ) 
         use MatrixOperations
         implicit none
         class(IPMethod_t)             :: self
         class(Face_t)                 :: face
         real(kind=RP), allocatable    :: T1(:,:) , T2(:,:) , J0(:,:) , J1(:,:)
!
!        -------------------------------------------------------
!           Computes the interior penalty face loop. Consists
!         in four terms:
!           T1= {du n}[[v]]
!           T2= -e{dv n}[[u]]
!           J0= [[u]][[v]]
!           J1= [[du]][[dv]]
!        -------------------------------------------------------
         select type ( face )
            type is (Face_t)
                 associate( uL => face % elements(LEFT)  % e % Qb(:,RIGHT) , &
                            uR => face % elements(RIGHT) % e % Qb(:,LEFT) , &
                           duL => face % elements(LEFT)  % e % dQb(:,RIGHT) , &
                           duR => face % elements(RIGHT) % e % dQb(:,LEFT) )
!
!                   -------------------------------------------------
!                    Terms added to the LEFT elements equation
!                   -------------------------------------------------
!
                    associate( eL => face % elements(LEFT) % e , &
                               eR => face % elements(RIGHT) % e )
                       allocate( T1( 0:eL % spA % N , NEC ) , T2( 0:eL % spA % N , NEC ) , J0 ( 0:eL % spA % N , NEC ) , J1( 0 : eL % spA % N , NEC ) )
                       
                       T1 = 0.5_RP * (face % n) * vectorOuterProduct( eL % spA % lb(:,RIGHT) , duL + duR )
                       T2 = -0.5_RP * self % epsilon * (1.0_RP / eL % hdiv2) * (face % n) * vectorOuterProduct( matmul( eL % spA % DT , eL % spA % lb(:,RIGHT) ) , uL - uR)
                       J0 = -0.5_RP * (self % sigma0)* (1.0_RP / el % hdiv2) * vectorOuterProduct( eL % spA % lb(:,RIGHT) , uL - uR )
                       J1 = -(self % sigma1)* vectorOuterProduct( matmul( eL % spA % DT , eL % spA % lb(:,RIGHT) ) , duL - duR )

                       eL % QDot = eL % QDot + viscousFlux(g = T1 + T2 + J0 + J1)

                       deallocate( T1 , T2 , J0 , J1 )

!
!                      -------------------------------------------------
!                         Terms added to the RIGHT element equation
!                      -------------------------------------------------
!
                       allocate( T1( 0:eR % spA % N , NEC ) , T2( 0:eR % spA % N , NEC ) , J0 ( 0:eR % spA % N , NEC ) , J1( 0 : eR % spA % N , NEC ) )

                       T1 = 0.5_RP * (-face % n) * vectorOuterProduct( eR % spA % lb(:,LEFT) , duL + duR )
                       T2 = -0.5_RP * self % epsilon * (1.0_RP / eR % hdiv2) * (face % n) * vectorOuterProduct( matmul( eR % spA % DT , eR % spA % lb(:,LEFT) ) , uL - uR)
                       J0 = 0.5_RP * (self % sigma0)* (1.0_RP / el % hdiv2) * vectorOuterProduct( eR % spA % lb(:,LEFT) , uL - uR )
                       J1 = (self % sigma1)* vectorOuterProduct( matmul( eR % spA % DT , eR % spA % lb(:,LEFT) ) , duL - duR )

                       eR % QDot = eR % QDot + viscousFlux(g = T1 + T2 + J0 + J1)

                       deallocate( T1 , T2 , J0 , J1 )

                    end associate
                    
                 end associate

            type is (BdryFace_t)
               associate( uE => face % elements(1) % e % Qb(:,face % BCLocation) , &
                          uB => face % uB , &
                         duE => face % elements(1) % e % dQb(:,face % BCLocation) , &
                         duB => face % gB , &
                           e => face % elements(1) % e)

                     allocate( T1( 0:e % spA % N , NEC ) , T2( 0:e % spA % N , NEC ) , J0 ( 0:e % spA % N , NEC ) , J1( 0 : e % spA % N , NEC ) )
     
                     T1 = 0.5_RP * (face % n) * vectorOuterProduct( e % spA % lb(:,face % BCLocation ) , duE + duB )
                     T2 = -0.5_RP * (self % epsilon) * (1.0_RP / e % hdiv2) * (face % n) * vectorOuterProduct( matmul( e % spA % DT , e % spA % lb(:,face % BCLocation) ) , uE - uB )
                     J0 = -0.5_RP * (self % sigma0) * (1.0_RP / e % hdiv2) * vectorOuterProduct( e % spA % lb(:,face % BCLocation) , uE - uB )
                     J1 = -(self % sigma1) * vectorOuterProduct( matmul( e % spA % DT , e % spA % lb(:, face % BCLocation) ) , duE - duB )
               
                     e % QDot = e % QDot + viscousFlux( T1 + T2 + J0 + J1 ) 

                     deallocate( T1 , T2 , J0 , J1 )

               end associate
            class default
         end select
     end subroutine IP_QDotFaceLoop

     subroutine IP_QDotVolumeLoop( self , element ) 
         use MatrixOperations
         implicit none
         class(IPMethod_t)             :: self
         class(Element1D_t)                 :: element
!
!        -------------------------------------------
!           Computes the IP volume loop:
!              IPVol = - tr(D)*M*dQel 
!        -------------------------------------------
!                                      
         element % QDot = element % QDot -  Mat_x_Mat_F ( element % spA % MD , viscousFlux(g = element % dQ) , trA = .true. )

     end subroutine IP_QDotVolumeLoop
!
!   ************************************************
!         BASSI-REBAY FLUXES
!   ************************************************
!
     subroutine BR1_dQFaceLoop( self , face ) 
         implicit none
         class(BR1Method_t)             :: self
         class(Face_t)                 :: face

         call average_dQFaceLoop( self , face )

     end subroutine BR1_dQFaceLoop

     subroutine BR1_QDotFaceLoop( self , face ) 
         use MatrixOperations
         implicit none
         class(BR1Method_t)                     :: self
         class(Face_t)                          :: face
         real(kind=RP), dimension(NEC)          :: gstar
!
!        Compute the averaged flux
         gstar = average_gFlux( face ) 
!
!        Perform the loop in both elements
         select type ( face )
            type is (Face_t)
               associate(QDot => face % elements(LEFT) % e % QDot, &
                   N=> face % elements(LEFT) % e % spA % N, &
                   e=> face % elements(LEFT) % e)

                  QDot = QDot + vectorOuterProduct(e % spA % lb(:,RIGHT) , gstar) * face % n

               end associate

               associate(QDot => face % elements(RIGHT) % e % QDot, &
                   N=> face % elements(RIGHT) % e % spA % N, &
                   e=> face % elements(RIGHT) % e)

                  QDot = QDot + vectorOuterProduct(e % spA % lb(:,LEFT) , gstar) * (-1.0_RP * face % n)

               end associate


            type is (BdryFace_t)
               associate(QDot => face % elements(1) % e % QDot , &
                  N => face % elements(1) % e % spA % N , &
                  e => face % elements(1) % e)

                  QDot = QDot + vectorOuterProduct(e % spA % lb(:,face % BCLocation) , gstar)*face % n
               end associate
            class default
         end select


         
     end subroutine BR1_QDotFaceLoop

     subroutine BR1_QDotVolumeLoop( self , element ) 
         use MatrixOperations
         implicit none
         class(BR1Method_t)             :: self
         class(Element1D_t)                 :: element

!        Perform the matrix multiplication
         associate( QDot => element % QDot , &
                    MD => element % spA % MD)

         QDot = QDot - Mat_x_Mat_F ( MD , viscousFlux( g = element % dQ ) , trA = .true. )

         end associate



     end subroutine BR1_QDotVolumeLoop


!
!    ****************************************************
!        Extra subroutines to compute fluxes
!    ****************************************************
!
     function average_uFlux( face ) result (val)
        implicit none
        class(Face_t)                  :: face
        real(kind=RP), dimension(NEC)  :: val
        real(kind=RP), pointer         :: QL(:) , QR(:) , QBdry(:)
!
!       Compute the average of both states
!
        select type ( face )
            type is (BdryFace_t)
               
                QBdry => face % elements(1) % e % Qb( : , face % BCLocation ) 
               
                val = 0.5_RP * (Qbdry + face % uB)

                QBdry => NULL()
            type is (Face_t)
                QL => face % elements(LEFT)  % e % Qb(: , RIGHT)
                QR => face % elements(RIGHT) % e % Qb(: , LEFT )
            
                val = 0.5_RP * (QL + QR) 

                QL => NULL()
                QR => NULL()
            class default
        end select

     end function average_uFlux

     function average_gFlux( face ) result ( val )
         implicit none
         class(Face_t)              :: face
         real(kind=RP), dimension(NEC)       :: val
         real(kind=RP), pointer        :: dQL(:) , dQR(:) , dQBdry(:)
!
!        Compute the average of both states
!
         select type ( face )
            type is ( BdryFace_t )
               
               dQBdry => face % elements(1) % e % dQb( : , face % BCLocation )

               val = 0.5_RP * (viscousFlux(g=dQBdry) + viscousFlux(g=face % gB))

               dQBdry => NULL()
            
            type is ( Face_t )
         
               dQL => face % elements(LEFT) % e % dQb( : , RIGHT )
               dQR => face % elements(RIGHT) % e %  dQb( : , LEFT ) 

               val = 0.5_RP * (viscousFlux(g=dQL) + viscousFlux(g=dQR) )
      
               dQL => NULL()
               dQR => NULL()

            class default

         end select

     end function average_gFlux
         




end module DGSecondOrderMethods
