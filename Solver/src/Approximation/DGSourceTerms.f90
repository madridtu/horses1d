module DGSourceTerms
   use SMConstants

   private
   public SourceTerm_t , NewSourceTerm
!
!  **************************************************
   integer, parameter      :: STR_LEN_SOURCES   = 128
!  **************************************************
!
!
!  ************************************
   real(kind=RP), protected  :: AF
   real(kind=RP), protected  :: dt
   integer      , protected  :: Ncutoff
!  ************************************
!
!  *******************************************************
   type SourceTerm_t
      character(len = STR_LEN_SOURCES)       :: SourceType
      procedure(SourceTermFCN), pointer      :: Apply
   end type SourceTerm_t
!  *******************************************************
!
   abstract interface 
      subroutine SourceTermFCN( self , mesh )
         use Mesh1DClass
         import SourceTerm_t
         implicit none
         class(SourceTerm_t)      :: self
         class(Mesh1D_t)          :: mesh
      end subroutine SourceTermFCN         
   end interface

!
!  ========
   contains
!  ========
!
      function NewSourceTerm() result( SourceTerm )
         use Setup_Class
         type (SourceTerm_t)           :: SourceTerm

         if ( len_trim ( Setup % SourceTerm ) .eq. 0 ) then
            SourceTerm % SourceType = "None"
            SourceTerm % Apply => SourceTerm_None
         
         elseif ( trim ( Setup % SourceTerm) .eq. "Turbulence" ) then
            SourceTerm % Apply => SourceTerm_Turbulence
      
            AF      = Setup % Turbulence_AF
            Ncutoff = Setup % Turbulence_Ncutoff
            dt      = Setup % dt

         else
            SourceTerm % SourceType = "None"
            SourceTerm % Apply => SourceTerm_None

         end if


      end function NewSourceTerm
!
!/////////////////////////////////////////////////////////////////////////////
!
!              SOURCE TERMS LIBRARY
!              --------------------
!/////////////////////////////////////////////////////////////////////////////
!
      subroutine SourceTerm_None ( self , mesh )
         use Mesh1DClass
         class ( SourceTerm_t )        :: self
         class ( Mesh1D_t     )        :: mesh

      end subroutine SourceTerm_None

      subroutine SourceTerm_Turbulence ( self , mesh )
         use Utilities
         use Mesh1DClass
         implicit none
         class ( SourceTerm_t )         :: self
         class ( Mesh1D_t     )         :: mesh
!        ---------------------------------------------------------------
         integer                        :: eID , k , wn 
         real(kind=RP), allocatable     :: sigma(:)

         allocate ( sigma(Ncutoff) ) 

         sigma = getGaussianNoise( Ncutoff )
         
         do eID = 1 , mesh % no_of_elements
            associate ( QDot => mesh % elements(eID) % QDot , x => mesh % elements(eID) % x )

            do k = 1 , Ncutoff
               wn = 2.0_RP * PI * k / ( mesh % elements( mesh % no_of_elements ) % nodes(RIGHT) % n % x - mesh % elements(1) % nodes(LEFT) % n % x ) 
               QDot(:,1) = QDot(:,1) + AF * sigma(k) * cos(wn*x) / sqrt(PI * dt * k)
            end do

            end associate
         end do

      end subroutine SourceTerm_Turbulence

end module DGSourceTerms
