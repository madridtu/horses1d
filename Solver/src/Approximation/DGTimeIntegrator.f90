module DGTimeIntegrator
   use SMConstants
   use DGSpatialDiscretizationMethods
   use Mesh1DClass
   use FileWriting

   private
   public TimeIntegrator_t , NewTimeIntegrator

   integer, parameter         :: STR_LEN_TIMEINTEGRATOR = 128

   type TimeIntegrator_t
      integer                               :: iter
      real(kind=RP)                         :: t
      real(kind=RP)                         :: dt
      real(kind=RP)                         :: t_end
      integer                               :: no_of_iterations
      integer                               :: initial_iteration
      integer                               :: plot_interval
      integer                               :: output_interval
      integer                               :: autosave_interval
      character(len=STR_LEN_TIMEINTEGRATOR) :: method
      integer                               :: mode
      procedure(TimeScheme), pointer, nopass :: TimeStep => NULL()
      contains
         procedure ::  Describe => TimeIntegrator_Describe
         procedure ::  Integrate => TimeIntegrator_Integrate
         procedure ::  Display  => TimeIntegrator_Display
         procedure ::  Autosave => TimeIntegrator_Autosave
   end type TimeIntegrator_t

   interface NewTimeIntegrator
      module procedure TimeIntegrator_NewTimeIntegrator 
   end interface NewTimeIntegrator

   abstract interface
      subroutine TimeScheme( mesh , dt , Storage)
         use SMConstants
         use Storage_module
         use Mesh1DClass
         implicit none
         class(Mesh1D_t)      :: mesh
         real(kind=RP)        :: dt
         class(Storage_t)     :: Storage
      end subroutine TimeScheme
   end interface

   
!  ========
   contains
!  ========
      function TimeIntegrator_NewTimeIntegrator() result (Integrator)
         use Setup_class
         implicit none
         type(TimeIntegrator_t)           :: Integrator

         
         if ( trim(Setup % IntegrationMode) .eq. "Steady") then
            Integrator % mode = STEADY
            Integrator % dt = Setup % dt
            Integrator % no_of_iterations = Setup % no_of_iterations 
            Integrator % t_end = (Setup % dt) * (Setup % no_of_iterations)
            Integrator % initial_iteration = Setup % InitialIteration
   
         elseif ( trim(Setup % IntegrationMode) .eq. "Transient") then
            Integrator % mode = TRANSIENT
            Integrator % dt = Setup % dt
            Integrator % no_of_iterations = ceiling( Integrator % t_end / Setup % dt )
            Integrator % t_end = Setup % no_of_iterations * Setup % dt
            
         else
            Stop "Stopped."

         end if

         Integrator % t = Setup % initialTime
         Integrator % output_interval = Setup % output_interval
         Integrator % autosave_interval = Setup % autosave_interval

         if ( trim(Setup % integrationMethod) .eq. "Explicit-Euler" ) then
            Integrator % TimeStep  => TimeIntegrator_ExplicitEuler

         elseif ( trim ( Setup % integrationMethod ) .eq. "Williamson RK3" ) then
            Integrator % TimeStep => TimeIntegrator_WilliamsonRK3

         elseif ( trim ( Setup % integrationMethod ) .eq. "Williamson RK5" ) then
            Integrator % TimeStep => TimeIntegrator_WilliamsonRK5

         else
            write(STD_OUT , *) "Method " , trim (Setup % integrationMethod) , " not implemented yet."

         end if
           

      end function TimeIntegrator_NewTimeIntegrator

      subroutine TimeIntegrator_Integrate( self , mesh , Storage )
         use Storage_module
         use Setup_Class
         implicit none
         class(TimeIntegrator_t)          :: self
         class(Mesh1D_t)                  :: mesh
         class(Storage_t)                 :: Storage
         integer                          :: iter

         do iter = self % initial_iteration + 1 , self % initial_iteration + self % no_of_iterations

            self % iter = iter

            call self % TimeStep( mesh , self % dt , Storage)
            self % t    = self % t + self % dt

            if ( iter .eq. 1 ) then
               call self % Display( mesh , Storage ) 

            elseif ( mod(iter , self % output_interval) .eq. 0 ) then
               call self % Display( mesh , Storage ) 

            end if

            if ( mod ( iter , self % autosave_interval ) .eq. 0 ) then
               call self % Autosave( Storage , mesh )

            end if


         end do

         call self % Autosave( Storage , mesh , trim( Setup % solution_file ) )

      end subroutine TimeIntegrator_Integrate
      
      subroutine TimeIntegrator_Display( self, mesh, Storage )
         implicit none  
         class(TimeIntegrator_t)          :: self
         class(Mesh1D_t)                  :: mesh
         class(Storage_t)                 :: Storage
         integer, parameter               :: ShowLabels = 50
         integer, save                    :: shown = 0

         if ( mod( shown , ShowLabels) .eq. 0 ) then     ! Show labels
            write(STD_OUT , '(/)')
            write(STD_OUT , '(/)')
            write(STD_OUT , '(A20,5X,A20,5X,A20)') "Iteration" , "Time" , "Residual"
            write(STD_OUT , '(A20,5X,A20,5X,A20)') "---------" , "----" , "--------"
         end if
         shown = shown + 1

         write(STD_OUT , '(I20,2X,A,2X,F20.8,2X,A,2X,F20.8)') self % iter ,"|", self % t ,"|", maxval(abs(Storage % QDot) )
      end subroutine TimeIntegrator_Display

      subroutine TimeIntegrator_Describe( self )
         implicit none
         class(TimeIntegrator_t)          :: self

         write(STD_OUT , *) "Time integrator description: "
         if (self % mode .eq. STEADY) write(STD_OUT , '(20X,A,A)') "Mode: " , "steady"
         if (self % mode .eq. TRANSIENT) write(STD_OUT , '(20X,A,A)') "Mode: " , "transient"
         write(STD_OUT ,'(20X,A,E10.3)') "Time step dt: " , self % dt
         write(STD_OUT , '(20X,A,I0)') "Number of iterations: " , self % no_of_iterations
         write(STD_OUT , '(20X,A,E10.3)') "Final simulation time: " , self % t_end
         
      end subroutine TimeIntegrator_Describe
   
      subroutine TimeIntegrator_Autosave( self , Storage , mesh , fileName_in)
         use Setup_class
         use Storage_module
         use NetCDFInterface
         use Physics
         class ( TimeIntegrator_t ) , intent  ( in )  :: self
         class ( Storage_t        ) , intent  ( in )  :: Storage
         class ( Mesh1D_t         ) , intent  ( in )  :: mesh
         character(len=*) , intent(in) , optional     :: fileName_in
!        ----------------------------------------------------------------
         character(len=STR_LEN_TIMEINTEGRATOR)            :: fileName
         real(kind=RP), allocatable                       :: x(:)
         integer                                          :: pos
         integer                                          :: counter , eID , i 

         if ( present(fileName_in) ) then
            fileName = fileName_in
            write(STD_OUT,'(/,20X,A,A,A)',advance="no") "** Saving solution file as ", trim(fileName) , "................."

         else
            fileName = trim(Setup % solution_file)
   
            pos = index(trim(fileName) , '.HiORst' )
            
            write(fileName, '(A,A,I0,A)') fileName(1:pos-1) , "_" , self % iter , ".HiORst" 
            
            write(STD_OUT,'(/,20X,A,A,A)',advance="no") "** Saving restart file as ", trim(fileName) , "................."
   
         end if

         allocate ( x , source = Storage % Q )

         call NetCDF_CreateFile ( trim(fileName) ) 
         call NetCDF_putDimension( trim(fileName) , "one" , 1 )
         call NetCDF_putDimension( trim(fileName) , "NDOF" , size(Storage % Q) )
         call NetCDF_putDimension( trim(fileName) , "N" , Setup % N )
         call NetCDF_putDimension( trim(fileName) , "no_of_elements" , mesh % no_of_elements )

         call NetCDF_putVariable( trim(fileName) , "t" , ["one"] , [self % t] )
   
         pos = max( index( Setup % saveVariables , '_Q' ) , index( Setup % saveVariables , 'Q_' ) )

         if ( pos .gt. 0 ) then
         
            call NetCDF_putVariable( trim(fileName) , "Q" , ["NDOF"] , Storage % Q )

         end if

         pos = max( index( Setup % saveVariables , '_QDot' ) , index( Setup % saveVariables , '_QDot' ) )

         if ( pos .gt. 0 ) then
         
            call NetCDF_putVariable( trim(fileName) , "QDot" , ["NDOF"] , Storage % QDot )

         end if

         pos = max( index( Setup % saveVariables , '_dQ' ) , index( Setup % saveVariables , 'dQ_' ) )

         if ( pos .gt. 0 ) then
         
            call NetCDF_putVariable( trim(fileName) , "dQ" , ["NDOF"] , Storage % dQ )

         end if

         call NetCDF_putVariable( trim(fileName) , "iter" , ["one"] , [self % iter] )

         counter = 0
         do eID = 1 , mesh % no_of_elements
            do i = 0 , mesh % elements(eID) % spA % N
               counter = counter + 1 
               x(counter) = mesh % elements(eID) % x(i)
            end do
         end do

         call NetCDF_putVariable( trim(fileName) , "x" , ["NDOF"] , x )
         
         write(STD_OUT , '(A,/)' ) "..  Saved"

         deallocate( x ) 


      end subroutine TimeIntegrator_Autosave
!
!
!     *******************************************************************************************************
!           Integration methods library
!     *******************************************************************************************************
!
      subroutine TimeIntegrator_ExplicitEuler( mesh , dt , Storage)
         use Storage_module
         implicit none
         class(Mesh1D_t)         :: mesh
         real(kind=RP)           :: dt
         class(Storage_t)        :: Storage
!
!        Compute the time derivative
!  
         call DGSpatial_computeTimeDerivative( mesh )
!
!        Perform a step in the explicit Euler method
!
         Storage % Q = Storage % Q + dt * Storage % QDot

      end subroutine TimeIntegrator_ExplicitEuler

      subroutine TimeIntegrator_WilliamsonRK3( mesh , dt , Storage )
         use Storage_module
         implicit none
         class(Mesh1D_t)          :: mesh
         real(kind=RP)              :: dt
         class(Storage_t)           :: Storage
!        -----------------------------------------
         real(kind=RP), allocatable, save :: G(:)
         integer                    :: m 
         integer, parameter         :: N_STAGES = 3
         real(kind=RP), parameter   :: am(3) = [0.0_RP , -5.0_RP / 9.0_RP , -153.0_RP / 128.0_RP]
         real(kind=RP), parameter   :: bm(3) = [0.0_RP , 1.0_RP / 3.0_RP  , 3.0_RP / 4.0_RP ]
         real(kind=RP), parameter   :: gm(3) = [1.0_RP / 3.0_RP , 15.0_RP / 16.0_RP , 8.0_RP / 15.0_RP ]
         
         if (.not. allocated(G) ) allocate ( G , source  = Storage % QDot )

         do m = 1 , N_STAGES
!
!           Compute time derivative
!           -----------------------
            call DGSpatial_ComputeTimeDerivative( mesh )

            if (m .eq. 1) then
               G = Storage % QDot
               Storage % Q = Storage % Q + gm(m) * dt * G
            else
               G = am(m) * G + Storage % QDot
               Storage % Q = Storage % Q + gm(m) * dt * G
            end if

         end do 

      end subroutine TimeIntegrator_WilliamsonRK3

      subroutine TimeIntegrator_WilliamsonRK5( mesh , dt , Storage )
!  
!        *****************************************************************************************
!           These coefficients have been extracted from the paper: "Fourth-Order 2N-Storage
!          Runge-Kutta Schemes", written by Mark H. Carpented and Christopher A. Kennedy
!        *****************************************************************************************
!
         use Storage_module
         implicit none
         class(Mesh1D_t)          :: mesh
         real(kind=RP)              :: dt
         class(Storage_t)           :: Storage
!        -----------------------------------------
         real(kind=RP), save, allocatable :: G(:)
         integer                    :: m 
         integer, parameter         :: N_STAGES = 5
         real(kind=RP), parameter  :: am(N_STAGES) = [0.0_RP , -0.4178904745_RP, -1.192151694643_RP , -1.697784692471_RP , -1.514183444257_RP ]
         real(kind=RP), parameter  :: gm(N_STAGES) = [0.1496590219993_RP , 0.3792103129999_RP , 0.8229550293869_RP , 0.6994504559488_RP , 0.1530572479681_RP]
         
         if (.not. allocated(G) ) allocate ( G , source  = Storage % QDot )

         do m = 1 , N_STAGES
!
!           Compute time derivative
!           -----------------------
            call DGSpatial_ComputeTimeDerivative( mesh )

            if (m .eq. 1) then
               G = dt * Storage % QDot
               Storage % Q = Storage % Q + gm(m) * G
            else
               G = am(m) * G + dt * Storage % QDot
               Storage % Q = Storage % Q + gm(m) * G
            end if

         end do 
         

      end subroutine TimeIntegrator_WilliamsonRK5

end module DGTimeIntegrator
