module MeshFileClass
    use SMConstants
    type MeshFile_t
       integer                        :: Npoints
       integer                        :: Nelements
       real(kind=RP), allocatable     :: nodes(:)
       integer, allocatable  :: elements(:,:)
       integer, allocatable  :: faceType(:) 
       integer, allocatable  :: polynomialOrder(:)
       integer, allocatable  :: cumulativePolynomialOrder(:)
       contains
            procedure ::      construct => NewMesh
    end type MeshFile_t

    private
    public  MeshFile_t

    contains

         subroutine NewMesh(mesh,K,xB)
             use Setup_class
             implicit none
             class(MeshFile_t)             :: mesh
             integer,          intent (in) :: K
             real(kind=RP),    intent (in) :: xB(2)
             real(kind=RP)                 :: T
             integer                       :: p , el
             real(kind=RP)                 :: h

             T = xB(RIGHT) - xB(LEFT)

             h = T / K
 
             mesh % Npoints = K+1
             mesh % Nelements = K

             allocate(mesh % nodes( mesh % Npoints ) )
             mesh % nodes = reshape((/(xB(LEFT) + h*p,p=0,K)/),(/K+1/))

             allocate(mesh % elements( mesh % Nelements , 2 ) )
             mesh % elements = reshape((/((el + p,p=1,0,-1),el=1,K)/),(/K , 2/), ORDER=(/2,1/))

             allocate(mesh % faceType( mesh % Npoints ) )
             mesh % faceType = FACE_INTERIOR
             mesh % faceType(1)              = 1
             mesh % faceType(mesh % Npoints) = 2

              

             allocate(mesh % polynomialOrder ( mesh % Nelements ) )
             mesh % polynomialOrder = setup % N

!       
!            ---------------------------------------------------------
!                   The cumulativePolynomialOrder is an array that 
!               goes from 0 to Nelements, and such that
!                   cumul..(0) = 0
!                   cumul..(i) = polynomialOrder(i) + cumul...(i-1)
!           ---------------------------------------------------------
             allocate(mesh % cumulativePolynomialOrder( 0 : mesh % Nelements ) )

            mesh % cumulativePolynomialOrder(0) = 0

            do el = 1 , mesh % Nelements
                mesh % cumulativePolynomialOrder(el) = mesh % cumulativePolynomialOrder(el-1) + mesh % polynomialOrder(el)
            end do

         end subroutine NewMesh  



end module MeshFileClass
