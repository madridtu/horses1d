!
! ///////////////////////////////////////////////////////////////////////////////////////
!
!     SMConstants.F
!
!!
!!     Modification History:
!!       version 0.0 August 10, 2005 David A. Kopriva
!
!     MODULE SMConstants
!
!!        Defines constants for use by the spectral demonstaration
!!        routines, including precision definitions. 
!
!!    @author David A. Kopriva
!
!////////////////////////////////////////////////////////////////////////////////////////
!
      module SMConstants
         use iso_fortran_env, only: real64 , real32

         integer       , parameter, private :: DOUBLE_DIGITS = 15                                  ! # of desired digits
         integer       , parameter, private :: SINGLE_DIGITS = 6                                   ! # of desired digits
         integer       , parameter          :: RP            = real64
         integer       , parameter          :: CP            = real64
         REAL(kind=RP) , parameter          :: PI            = 3.141592653589793238462643_RP
            
         integer, parameter                 :: FORWARD       = +1
         integer, parameter                 :: BACKWARD      = -1
            
         integer, parameter                 :: RIGHT         = 1
         integer, parameter                 :: LEFT          = 2
            
         integer, parameter                 :: LG            = 1               ! Parameter for Legendre-Gauss nodes
         integer, parameter                 :: LGL           = 2               ! Parameter for Legendre-Gauss-Lobatto nodes
   
         integer, parameter                 :: STD_OUT       = 6
         integer, parameter                 :: STD_IN        = 5
         integer, parameter                 :: LINE_LENGTH   = 132
            
         complex(kind=CP)                   :: ImgI          = ( 0.0_RP, 1.0_RP)                   !                     = SQRT(-1.0_RP)
   
   
         integer, parameter                 :: FACE_INTERIOR = 0
         integer, parameter                 :: FACE_BOUNDARY = 1
   
         integer, parameter                 :: PERIODIC_BC = 0
         integer, parameter                 :: DIRICHLET_BC = 1
            
         integer, parameter                 :: STEADY = 0
         integer, parameter                 :: TRANSIENT = 1
   
      end module SMConstants
