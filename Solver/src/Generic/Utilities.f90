module Utilities
   use SMConstants
   implicit none

!
!  ========
   contains
!  ========
!
!//////////////////////////////////////////////////////////////////////////////////////////
!
      function getGaussianNoise( N ) result ( sigma )
         implicit none
         integer, intent(in)     :: N
         real(kind=RP)           :: sigma ( N )
!        -----------------------------------------------------
         integer                 :: N_even
         real(kind=RP)           :: r1 , r2
         integer                 :: i

         N_even = ( N / 2 )

         do i = 1 , N_even
            call random_number ( r1 )
            call random_number ( r2 )

            sigma(2*i-1) = sqrt( -2.0_RP * log( r1 ) ) * cos ( 2.0_RP * PI * r2 )
            sigma(2*i  ) = sqrt( -2.0_RP * log( r1 ) ) * sin ( 2.0_RP * PI * r2 )
            
         end do

         if ( N_even * 2 .ne. N ) then
            call random_number ( r1 ) 
            call random_number ( r2 )

            sigma ( N ) = sqrt( -2.0_RP * log( r1 ) ) * cos ( 2.0_RP * PI * r2 )

         end if

      end function getGaussianNoise
!
!/////////////////////////////////////////////////////////////////////////////////////////////
!
!
!
end module Utilities
