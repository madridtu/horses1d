module Setup_class
    use SMConstants
    use ParamfileIO
    implicit none

    private
    public  :: setup
  
    integer, parameter          :: STR_LEN_SETUP = 128
    type Setup_t
!
!       -------------------------------------------------------------------------------------
!              Spatial discretization parameters
!       -------------------------------------------------------------------------------------
!
        integer                      :: nodes                      !  Interpolation / Integration nodes strategy
        integer, allocatable         :: K                          ! Number of elements
        integer, allocatable         :: N                          ! Polynomial order (generic)
        real(kind=RP)                :: xB(RIGHT:LEFT)
        real(kind=RP)                :: T                          ! Domain size
!
!       --------------------------------------------------------------------------------------
!              Initialization
!       --------------------------------------------------------------------------------------
!
        character(len=STR_LEN_SETUP) :: IC          ! Initial condition type
        character(len=STR_LEN_SETUP) :: restart_file
        real(kind=RP), allocatable   :: a0          ! Initial condition parameter
        real(kind=RP), allocatable   :: ampIC       ! Amplitude
        real(kind=RP), allocatable   :: thetaIC     ! Phase
        real(kind=RP), allocatable   :: nIC         ! Wavenumber
        real(kind=RP)                :: rampPoints(2)
!
!       -----------------------------------------------------------------------------------------
!              Advective flux discretization
!       -----------------------------------------------------------------------------------------
!
        character(len=STR_LEN_SETUP) :: inviscid_discretization
        character(len=STR_LEN_SETUP) :: riemann_solver            ! Riemann solver
        integer, allocatable         :: integration_points        ! Number of integration points

!     
!       -------------------------------------------------------------------------------------------
!              Viscous discretization
!       -------------------------------------------------------------------------------------------
!
        real(kind=RP), allocatable   :: nu                         ! Viscous coefficient
        character(len=STR_LEN_SETUP) :: viscous_discretization   
        character(len=STR_LEN_SETUP) :: IPMethod                 
        real(kind=RP), allocatable   :: sigma0IP                 
        real(kind=RP), allocatable   :: sigma1IP                 
!
!       ---------------------------------------------------------------------------------------
!              Source Terms
!       ---------------------------------------------------------------------------------------
!
         character(len=STR_LEN_SETUP) :: SourceTerm
         real(kind=RP), allocatable   :: Turbulence_AF
         integer      , allocatable   :: Turbulence_Ncutoff
!
!       ---------------------------------------------------------------------------------------
!              Boundary conditions parameters
!       ---------------------------------------------------------------------------------------
!
        character(len=STR_LEN_SETUP) :: bdry_file
        integer, dimension(2)        :: markers                   =  [1,2]
!
!       ------------------------------------------------------------------------------
!              Integration parameters
!       ------------------------------------------------------------------------------
!
        character(len=STR_LEN_SETUP) :: integrationMode         
        real(kind=RP), allocatable   :: dt                      
        real(kind=RP), allocatable   :: simulationTime          
        integer, allocatable         :: no_of_iterations        
        real(kind=RP), allocatable   :: initialTime             
        integer                      :: initialIteration
        character(len=STR_LEN_SETUP) :: integrationMethod       
!
!       ------------------------------------------------------------------------------
!             Output parameters
!       ------------------------------------------------------------------------------
!
        integer, allocatable         :: autosave_interval        
        integer, allocatable         :: output_interval          
        character(len=STR_LEN_SETUP) :: saveVariables         
        character(len=STR_LEN_SETUP) :: solution_file

        contains
            procedure   :: Initialization => Setup_Initialization 
            procedure   :: SetInitialtime => Setup_SetInitialTime
    end type Setup_t

    type(Setup_t), protected, target       :: setup

    contains

      subroutine Setup_Initialization( self )
         implicit none
         class(Setup_t)                :: self
         integer                       :: nArgs
         character(len=STR_LEN_SETUP)  :: arg
         integer                       :: iArg
         integer                       :: pos
         character(len=STR_LEN_SETUP)  :: case_name
         character(len=STR_LEN_SETUP)  :: interp_nodes
         character(len=STR_LEN_SETUP)  :: inviscid_form
         real(kind=RP), allocatable    :: xL , xR , rL , rR
!
!        Get case file from command line
!        -------------------------------
         nArgs = command_argument_count()

         if ( nArgs .eq. 0 ) then
            write(STD_OUT , '(/,/)')
            print*, "No case file(s) selected"
            stop "Stopped"
         end if

         do iArg = 1 , nArgs
            call get_command_argument(iArg , arg)
            
            pos = index( trim(arg) , '.HiOCase' )
         
            if ( pos .gt. 0 ) then
               case_name= trim(arg)
               exit
            elseif ( iArg .eq. nArgs ) then
               write(STD_OUT,'(/,/)')
                  print*, "No case file(s) selected"
                  stop "Stopped"
            end if
   
         end do
!
!        Read from case file
!        -------------------
         call readValue ( trim(case_name) , "Left boundary position" , xL ) 
         call readValue ( trim(case_name) , "Right boundary position" , xR ) 

         call readValue ( trim(case_name) , "Boundary file" , Setup % bdry_file )

         if ( trim(Setup % bdry_file) .eq. "[self]" ) Setup % bdry_file = case_name

         if ( allocated(xL) .and. allocated(xR) ) then
            Setup % xB(LEFT)  = xL
            Setup % xB(RIGHT) = xR
            Setup % T         = xR - xL
            if ( Setup % T .lt. 0.0_RP ) then
               stop "Negative domain size"
            end if

         end if

         call readValue( trim(case_name) , "Interpolation nodes" , interp_nodes ) 
         call readValue( trim(case_name) , "Polynomial order" , self % N ) 

         if ( trim(interp_nodes) .eq. "Legendre-Gauss") then
            Setup % nodes = LG

         elseif ( trim(interp_nodes) .eq. "Legendre-Gauss-Lobatto") then
            Setup % nodes = LGL

         end if

         call readValue( trim(case_name) , "Number of elements" , Setup % K )
         call readValue( trim(case_name) , "Initial condition" , Setup % IC )
         call readValue( trim(case_name) , "Restart file" , Setup % restart_file )
         call readValue( trim(case_name) , "Initial constant value" , Setup % a0 ) 
         call readValue( trim(case_name) , "Oscillation amplitude" , Setup % ampIC ) 
         call readValue( trim(case_name) , "Initial phase" , Setup % thetaIC ) 
         call readValue( trim(case_name) , "Initial wavenumber" , Setup % nIC ) 
         call readValue( trim(case_name) , "Ramp left value" , rL ) 
         call readValue( trim(case_name) , "Ramp right value" , rR ) 

         if ( allocated(rL) .and. allocated(rR) ) then
                Setup % rampPoints(LEFT) = rL
                Setup % rampPoints(RIGHT) = rR
         end if

         call readValue( trim(case_name) , "Inviscid discretization" , Setup % inviscid_discretization ) 
         call readValue( trim(case_name) , "Riemann solver" , Setup % Riemann_solver ) 
         call readValue( trim(case_name) , "Number of integration points" , Setup % integration_points ) 
         
         call readValue( trim(case_name) , "Viscosity" , Setup % nu ) 
         call readValue( trim(case_name) , "Viscous discretization" , Setup % viscous_discretization ) 
         call readValue( trim(case_name) , "Interior penalty method" , Setup % IPMethod ) 
         call readValue( trim(case_name) , "Jumps penalty parameter" , Setup % Sigma0IP ) 
         call readValue( trim(case_name) , "Gradients jump penalty parameter" , Setup % Sigma1IP ) 

         call readValue ( trim ( case_name )  , "Source term"                  , Setup % SourceTerm         ) 
         call readValue ( trim ( case_name )  , "Turbulence amplitude"         , Setup % Turbulence_AF      ) 
         call readValue ( trim ( case_name )  , "Turbulence cutoff wavenumber" , Setup % Turbulence_Ncutoff ) 
   
         call readValue ( trim(case_name) , "Integration mode" , Setup % integrationMode)
         call readValue ( trim(case_name) , "Integration scheme" , Setup % integrationMethod)
         call readValue ( trim(case_name) , "Time step" , Setup % dt)
         call readValue ( trim(case_name) , "Simulation time" , Setup % simulationTime)
         call readValue ( trim(case_name) , "Initial time" , Setup % initialtime)
         call readValue ( trim(case_name) , "Number of iterations" , Setup % no_of_iterations)
         
         call readValue ( trim(case_name) , "Autosave interval" , Setup % autosave_Interval ) 
         call readValue ( trim(case_name) , "Output interval" , Setup % output_Interval ) 
         call readValue ( trim(case_name) , "Save variables" , Setup % saveVariables )
         call readValue ( trim(case_name) , "Solution file" , Setup % solution_file )
          
      end subroutine Setup_Initialization
         
      subroutine Setup_SetInitialTime( self , t , iter )
         implicit none
         class(Setup_t)          :: self
         real(kind=RP)           :: t
         integer                 :: iter

         self % initialTime = t
         self % initialIteration = iter

      end subroutine Setup_SetInitialTime
         
         
        
end module Setup_class
